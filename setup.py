from setuptools import setup, find_namespace_packages

setup(
      name='tg-x-estate',
      version='0.0.1',
      packages=find_namespace_packages(where='src'),
      package_dir={"": "src"},
      setup_requires=['protobuf_distutils==1.0'],
      install_requires=[
            'telethon==1.24.0',
            'asyncio==3.4.3',
            'protobuf==3.19.3',
            'parameterized',
            'regex',
            'requests',
            'beautifulsoup4',
            'pymongo',
            'fastapi',
            'uvicorn[standard]',
            'jupyter',
            'pandas',
            'sklearn',
            'seaborn',
            'numpy',
            'yellowbrick'
      ],
      options={
            'generate_py_protobufs': {
                  'source_dir': 'src/messaging',
                  'extra_proto_paths': [],
                  'output_dir': 'src/messaging/generated/',
                  'proto_files': [
                        'src/messaging/Address.proto',
                        'src/messaging/Building.proto',
                        'src/messaging/BuildingMaterial.proto',
                        'src/messaging/Currency.proto',
                        'src/messaging/Price.proto',
                        'src/messaging/SaleAd.proto',
                        'src/messaging/SaleAdFoundEvent.proto',
                        'src/messaging/Source.proto',
                  ],
            },
      },
)
