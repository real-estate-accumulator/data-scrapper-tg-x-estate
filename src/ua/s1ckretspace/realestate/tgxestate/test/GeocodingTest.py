import json
import unittest

# flat_sale_ad = json.loads(
#     """
#     {
#         "address": {
#             "city": "\u0425\u0430\u0440\u043a\u0456\u0432",
#             "country": "\u0423\u043a\u0440\u0430\u0457\u043d\u0430",
#             "house_number": "138",
#             "street": "\u043f\u0440. \u0413\u0435\u0440\u043e\u0435\u0432 \u0421\u0442\u0430\u043b\u0438\u043d\u0433\u0440\u0430\u0434\u0430"
#         },
#         "area": 39.0,
#         "at_floor": 1,
#         "building_max_floor": 9,
#         "description": "\u26a1\ufe0f177205\n\ud83d\udd111\u043a\n\u270f\ufe0f39.0\u043c\u00b2\n\ud83d\udd3a\u042d\u0442\u0430\u0436: 1/9 \u26a0\ufe0f\n\ud83d\udccd\u041d\u043e\u0432\u044b\u0435 \u0414\u043e\u043c\u0430, \u043f\u0440. \u0413\u0435\u0440\u043e\u0435\u0432 \u0421\u0442\u0430\u043b\u0438\u043d\u0433\u0440\u0430\u0434\u0430 138\n\ud83d\udcb535000 $\n#\u041d\u043e\u0432\u044b\u0435\u0414\u043e\u043c\u04301\u043a\n#\u041d\u043e\u0432\u044b\u0435\u0414\u043e\u043c\u04301\u043a\u0412\u0442\u043e\u0440\u0438\u0447\u043a\u0430",
#         "image_urls": [
#             "https://kvartirant-photos.s3.eu-central-1.amazonaws.com/177205/oZAQ6SEm1fgaeoyVQ2I36OH3Ux3OcDNCGPY5.jpg",
#             "https://kvartirant-photos.s3.eu-central-1.amazonaws.com/177205/toyFLB0D4JnSwOtL46rRgcFlsGKiyEMuW9jc.jpg",
#             "https://kvartirant-photos.s3.eu-central-1.amazonaws.com/177205/pAXuQPWLoSjjwWoiEo0YakwAtud2T678huiE.jpg",
#             "https://kvartirant-photos.s3.eu-central-1.amazonaws.com/177205/lm88sKI9SaVEroDOxYj3UySZE0zgSgGzMNq2.jpg",
#             "https://kvartirant-photos.s3.eu-central-1.amazonaws.com/177205/76dDL3Sk0u1KMjmEtDvqY0FbkCawRQPMu92C.jpg",
#             "https://kvartirant-photos.s3.eu-central-1.amazonaws.com/177205/m8ESxebeZcibCVJsTL7eMHHPJaSVNRHqpE4m.jpg",
#             "https://kvartirant-photos.s3.eu-central-1.amazonaws.com/177205/n038n7N9s87Ch6IkMnS95RC18DoxRDyaRCxR.jpg",
#             "https://kvartirant-photos.s3.eu-central-1.amazonaws.com/177205/M5AEST1YI7NP2T1zJkIbJV8GlmPvCHxf75ET.jpg"
#         ],
#         "price": {
#             "amount": 35000,
#             "currency": "$"
#         },
#         "ready_for_living": true,
#         "room_qty": 1,
#         "source": {
#             "origin": "TG_X_ESTATE",
#             "url": "https://t.me/x_prodazha_kvartir_kharkov/108492"
#         },
#         "title": "\u26a1\ufe0f177205\n\ud83d\udd111\u041a\n\u270f\ufe0f39.0\u041c\u00b2\n\ud83d\udd3a\u042d\u0442\u0430\u0436: 1/9 \u26a0\ufe0f\n\ud83d\udccd\u041d\u043e\u0432\u044b\u0435 \u0414\u043e\u043c\u0430, \u041f\u0440. \u0413\u0435\u0440\u043e\u0435\u0432 \u0421\u0442\u0430\u043b\u0438\u043d\u0433\u0440\u0430\u0434\u0430 138\n\ud83d\udcb535000 $\n#\u041d\u043e\u0432\u044b\u0435\u0434\u043e\u043c\u04301\u041a\n#\u041d\u043e\u0432\u044b\u0435\u0434\u043e\u043c\u04301\u041a\u0432\u0442\u043e\u0440\u0438\u0447\u043a\u0430"
#     }
#     """
import requests


class GeocodingTest(unittest.TestCase):

    def test_parse_message_body(self):
        flat_sale_ad = json.loads(
            """
            {
                "address": {
                    "city": "\u0425\u0430\u0440\u043a\u0456\u0432",
                    "country": "\u0423\u043a\u0440\u0430\u0457\u043d\u0430",
                    "house_number": "138",
                    "street": "\u043f\u0440. \u0413\u0435\u0440\u043e\u0435\u0432 \u0421\u0442\u0430\u043b\u0438\u043d\u0433\u0440\u0430\u0434\u0430"
                }
            }
            """
        )

        street = f"{flat_sale_ad['address']['street']} {flat_sale_ad['address']['house_number']}".replace(" ", "+")
        print(street)
        url = f"https://nominatim.openstreetmap.org/search?format=json&addressdetails=1&polygon_geojson=1&street={street}&city=Харків&country=Україна"
        print(url)
        result = requests.get(url).json()
        if len(result) == 0:
            print("No results found!")
            # save to not-found db
            return

        result = result[0]
        lat = result.get('lat')
        lon = result.get('lon')
        address = result.get('address')
        geojson = result.get('geojson')
        print(lat)
        print(lon)
        print(address)
        print(geojson)
        # save to
