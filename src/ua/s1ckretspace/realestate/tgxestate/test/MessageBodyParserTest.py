import unittest

from parameterized import parameterized

from ..x_estate_data_extractor import parse_message_body


class MessageBodyParserTest(unittest.TestCase):

    @parameterized.expand([
        ["⚡️177205\n🔑1к\n✏️39.0м²\n🔺Этаж: 1/9 ⚠️\n📍Новые Дома, пр. Героев Сталинграда 138\n💵35000 $\n#НовыеДома1к\n#НовыеДома1кВторичка",
         {}]
    ])
    def test_parse_message_body(self, message_body, result):
        d = parse_message_body(message_body)
        print(d)
