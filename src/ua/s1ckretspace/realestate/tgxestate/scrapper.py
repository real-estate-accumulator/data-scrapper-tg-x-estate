import datetime

import pymongo
from pymongo.collection import Collection
from telethon import TelegramClient
from telethon.tl.types import Channel

from src.ua.s1ckretspace.realestate.tgxestate.parser import parse_message, geocode
from src.ua.s1ckretspace.realestate.tgxestate.env import tg_api_id, tg_api_hash, tg_channel_link, mongo_connect_url, mongo_db_name, \
    mongo_building_collection_name, mongo_sale_ads_collection_name, mongo_sale_ads_404_collection_name


class XEstateScrapper:
    def __init__(self,
                 tg_client: TelegramClient,
                 tg_channel_uri: str,
                 building_collection: Collection,
                 sale_ads_collection: Collection,
                 sale_ads_404_collection: Collection):
        self.tg_client = tg_client
        self.tg_channel_uri = tg_channel_uri
        self.building_collection = building_collection
        self.sale_ads_collection = sale_ads_collection
        self.sale_ads_404_collection = sale_ads_404_collection

    async def scrape_ads(self, from_date: datetime.datetime, to_date: datetime.datetime):
        # Note: messages are in UTC
        channel: Channel = await self.tg_client.get_entity(self.tg_channel_uri)
        async for message in self.tg_client.iter_messages(entity=channel, offset_date=from_date, reverse=True):
            try:
                flat_sale_ad = parse_message(self.tg_channel_uri, message)
            except Exception as e:
                print("Error while parsing", e)
                continue

            print(f"Found sale ad from {flat_sale_ad['posted_at']}")
            if flat_sale_ad['posted_at'] > int(to_date.timestamp()):
                print(f"Stopping execution! Reached to_data {to_date}")
                exit(0)

            # Geocoding
            try:
                street = flat_sale_ad['address']['street']
                house_number = flat_sale_ad['address']['house_number']
                geocode_result = geocode(street, house_number)
            except Exception as e:
                print("Error while geocoding", e)
                continue

            try:
                if geocode_result is not None and geocode_result['geojson']['type'] == 'Polygon':
                    # Drop original address because we got more detailed one from geocoding
                    del flat_sale_ad['address']

                    print("Saving to buildings")
                    building_result = self.building_collection.update_one(
                        filter={"osm_id": geocode_result["osm_id"]},
                        update={
                            "$set": {
                                **geocode_result,
                                "floor_qty": flat_sale_ad["building_max_floor"],
                                "found_at": flat_sale_ad["found_at"],
                                "posted_at": flat_sale_ad["posted_at"]
                            }
                        },
                        upsert=True
                    )
                    building_id = building_result.upserted_id
                    if building_id is None:
                        building_id = self.building_collection.find_one({"osm_id": geocode_result["osm_id"]}, {"_id": 1})["_id"]
                        print("Building already present", building_id)
                    else:
                        print("Saved to buildings", building_id)

                    print("Saving to sale_ads")
                    # TODO: If the same ad is found update price history, source links, etc. (After MVP)
                    sale_ad_result = self.sale_ads_collection.insert_one(
                        {
                            "building_id": building_id,
                            **flat_sale_ad,
                        }
                    )
                    print("Saved to sale_ads", sale_ad_result.inserted_id)
                else:
                    print("Saving to sale_ads_404")
                    result = self.sale_ads_404_collection.insert_one(flat_sale_ad)
                    print("Saved to sale_ads_404", str(result.inserted_id))
            except Exception as e:
                print("Error while saving", e)
                continue


if __name__ == '__main__':
    mongo_client: pymongo.MongoClient = pymongo.MongoClient(mongo_connect_url)
    restate_db = mongo_client[mongo_db_name]
    building_collection = restate_db[mongo_building_collection_name]
    sale_ads_collection = restate_db[mongo_sale_ads_collection_name]
    sale_ads_404_collection = restate_db[mongo_sale_ads_404_collection_name]

    tg_client: TelegramClient = TelegramClient('x_estate_data_extractor', tg_api_id, tg_api_hash)
    from_date = datetime.datetime.strptime("01-01-2022", "%d-%m-%Y")
    to_date = datetime.datetime.strptime("02-01-2022", "%d-%m-%Y")

    x_estate_scrapper = XEstateScrapper(tg_client=tg_client,
                                        tg_channel_uri=tg_channel_link,
                                        building_collection=building_collection,
                                        sale_ads_collection=sale_ads_collection,
                                        sale_ads_404_collection=sale_ads_404_collection)

    with tg_client:
        tg_client.loop.run_until_complete(x_estate_scrapper.scrape_ads(from_date, to_date))
