import os

tg_channel_link: str = os.getenv("RE_TG_LINK")
tg_api_id: int = int(os.getenv("RE_TG_API_ID"))
tg_api_hash: str = os.getenv("RE_TG_API_HASH")

mongo_connect_url: str = os.getenv("RE_MONGO_CONNECT_URL")
mongo_db_name: str = os.getenv("RE_MONGO_DB_NAME")
mongo_building_collection_name: str = "buildings"
mongo_sale_ads_collection_name: str = "sale_ads"
mongo_sale_ads_404_collection_name: str = "sale_ads_404"
