import pymongo
from bson import ObjectId
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from src.ua.s1ckretspace.realestate.tgxestate.env import mongo_connect_url, mongo_db_name, mongo_sale_ads_404_collection_name, \
    mongo_sale_ads_collection_name, mongo_building_collection_name

app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

mongo_client: pymongo.MongoClient = pymongo.MongoClient(mongo_connect_url)
restate_db = mongo_client[mongo_db_name]
building_collection = restate_db[mongo_building_collection_name]
sale_ads_collection = restate_db[mongo_sale_ads_collection_name]
sale_ads_404_collection = restate_db[mongo_sale_ads_404_collection_name]


@app.get("/api/v1/buildings_polygons")
def get_all_buildings_with_sale_ads():
    buildings = list(building_collection.find({}, {"_id": 1, "geojson": 1}))
    for b in buildings:
        b['_id'] = str(b['_id'])
    return buildings


@app.get("/api/v1/buildings")
def get_all_buildings_with_sale_ads():
    buildings = list(building_collection.find({}, {"_id": 1, "lat": 1, "lon": 1}))
    for b in buildings:
        b['_id'] = str(b['_id'])
    return buildings


@app.get("/api/v1/buildings/{building_id}")
def get_building_sale_ads(building_id: str):
    sale_ads = list(sale_ads_collection.aggregate([
        {"$match": {"building_id": ObjectId(building_id)}},
        {"$lookup": {
            "from": "buildings",
            "localField": "building_id",
            "foreignField": "_id",
            "pipeline": [
                {
                    "$project": {
                        "address": 1,
                    }
                }
            ],
            "as": "building"
        }},
        {"$unwind": "$building"},
        {"$project": {
            "_id": 1,
            "room_qty": 1,
            "total_area": 1,
            "at_floor": 1,
            "building_max_floor": 1,
            "ready_for_living": 1,
            "price": 1,
            "photo_urls": 1,
            "title": 1,
            "description": 1,
            "posted_at": 1,
            "found_at": 1,
            "source": 1,
            "image_urls": 1,
            "condition": 1,
            "classified_by": 1,
            "address": "$building.address",
        }}
    ]))
    for s in sale_ads:
        s['_id'] = str(s['_id'])
    return sale_ads


@app.get("/api/v1/sale_ads")
def get_sale_ads_by_building_id(building_id: str):
    sale_ads = list(sale_ads_collection.find({"building_id": ObjectId(building_id)}))
    for s in sale_ads:
        s['_id'] = str(s['_id'])
        s['building_id'] = str(s['building_id'])
    return sale_ads
