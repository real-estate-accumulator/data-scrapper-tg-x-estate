import datetime

import regex
import requests
from bs4 import BeautifulSoup
from telethon.tl.types import Message


def extract_id(text):
    match = regex.search("⚡️(\d*)", text)
    return int(match.group(1))


def extract_room_qty(text):
    if (regex.search("Гостинка", text)) is not None:
        return 1
    match = regex.search("([1-9])к", text)
    return int(match.group(1))


def extract_area(text):
    match = regex.search("([\d.]*)м²", text)
    return float(match.group(1))


def extract_floor(text):
    match = regex.search("(\d*)/(\d*)", text)
    return int(match.group(1)), int(match.group(2))


def extract_price(text):
    match = regex.search("(\d*) ([\$€]|грн)", text)
    return int(match.group(1)), match.group(2)


def extract_street(text):
    match = regex.search("((пр\.|ул\.|пер\.)([\p{L} ]{1,}))([\d][\d\p{L}\/\-\.]*)", text)
    return str(match.group(3)).strip(), str(match.group(4)).strip()


def extract_living_state(text):
    match = regex.search("Строительное состояние", text)
    if match:
        return False
    else:
        return True


def parse_message_body(text: str):
    print("Parsing message body", text.replace('\n', ' | '))
    id: int = extract_id(text)
    room_qty: int = extract_room_qty(text)
    area: float = extract_area(text)
    at_floor, building_max_floor = extract_floor(text)
    amount, currency = extract_price(text)
    street, house_number = extract_street(text)
    ready_for_living = extract_living_state(text)
    return {
        "source_id": id,
        "room_qty": room_qty,
        "total_area": area,
        "at_floor": at_floor,
        "building_max_floor": building_max_floor,
        "price": {
            "currency": currency,
            "amount": amount
        },
        "address": {
            "country": "Україна",
            "city": "Харків",
            "street": street,
            "house_number": house_number
        },
        "ready_for_living": ready_for_living
    }


def get_post_title(soup):
    header_tag = soup.find('h1')
    return header_tag.text.strip()


def get_image_urls(soup):
    image_tags = soup.findAll('img')
    return [img['src'] for img in image_tags]


def parse_webpage(webpage_url):
    webpage = requests.get(webpage_url)
    soup = BeautifulSoup(webpage.content, "html.parser")
    return get_post_title(soup), get_image_urls(soup)


def parse_message(channel_link: str, message: Message):
    ad_attributes = parse_message_body(message.message)
    title, image_urls = parse_webpage(message.media.webpage.url)
    description = message.message
    source_url = f"{channel_link}/{message.id}"
    return {
        "title": title,
        "description": description,
        "posted_at": int(message.date.timestamp()),
        "found_at": int(datetime.datetime.now().timestamp()),
        "source": {
            "origin": "TG_X_ESTATE",
            "url": source_url,
        },
        "image_urls": image_urls,
        **ad_attributes
    }


def geocode(street, house_number):
    street = f"{street} {house_number}".replace(" ", "+")
    url = f"https://nominatim.openstreetmap.org/search?email=ivan.kovalenko1@nure.ua&format=json&addressdetails=1&polygon_geojson=1&street={street}&city=Харків&country=Україна"
    print(f"Geocoding {street}")
    result = requests.get(url).json()
    if len(result) == 0:
        print("No results found!")
        return None

    result = result[0]
    osm_type = result.get('osm_type')
    osm_id = result.get('osm_id')
    if osm_type != 'way':
        print(f"OSM type '{osm_type}' != way for id {osm_id}! Returning none.")
        return None

    lat = float(result.get('lat'))
    lon = float(result.get('lon'))
    address = result.get('address')
    geojson = result.get('geojson')
    print(f"Found a result {result.get('display_name')}")
    return {
        "osm_id": f"W{osm_id}",
        "lat": lat,
        "lon": lon,
        "address": address,
        "geojson": geojson
    }
