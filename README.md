# data-scrapper-tg-x-estate

Scrapes data from Telegram channel (X-Estate https://t.me/x_prodazha_kvartir_kharkov)

## How to build

```shell
. venv/bin/activate
pip install -e .
python setup.py generate_py_protobufs
```

## How to run

You should complete 3 phases to run the application.

### Phase I. Configuration

You **MUST** create your own `res/local.env` config.

#### Set up app configuration

```shell
set -o allexport
source ./res/local.env
set +o allexport
```

### Phase II. Infrastructure

#### Create things for MongoDB

```shell
docker network create restate

docker volume create restate-mongo-data
```

#### Create MongoDB

```shell
docker run -d \
    -p $RE_MONGO_PORT:$RE_MONGO_PORT \
    --name restate-mongo \
    --network restate \
    -v restate-mongo-data:/data/db \
    -e MONGO_INITDB_ROOT_USERNAME=$RE_MONGO_USERNAME \
    -e MONGO_INITDB_ROOT_PASSWORD=$RE_MONGO_PASSWORD \
    mongo
```

#### Start mongo

```shell
docker start restate-mongo
```

#### (Optional) Create MongoExpress

```shell
docker run -d \
    -p 8081:8081 \
    --name restate-mongo-express \
    --network restate \
    -e ME_CONFIG_MONGODB_ADMINUSERNAME=$RE_MONGO_USERNAME \
    -e ME_CONFIG_MONGODB_ADMINPASSWORD=$RE_MONGO_PASSWORD \
    -e ME_CONFIG_MONGODB_URL="mongodb://$RE_MONGO_USERNAME:$RE_MONGO_PASSWORD@restate-mongo:$RE_MONGO_PORT/$RE_MONGO_DB_NAME" \
    mongo-express
```

#### (Optional) Start MongoExpress

```shell
docker start restate-mongo-express
```

### Phase III. Application

#### Run web server

```shell
uvicorn src.ua.s1ckretspace.realestate.tgxestate.webserver:app --reload
```

## App Configuration

App configuration is performed ONLY through environment variables.

Please use this [markdown table generator](https://www.tablesgenerator.com/markdown_tables) to update this table.


| ENV var name         | ENV var description                                                                                |
|----------------------|----------------------------------------------------------------------------------------------------|
| RE_TG_LINK           | Telegram channel link (note: the app is designed for XEstate only)                                 |
| RE_TG_API_ID         | Telegram API id                                                                                    |
| RE_TG_API_HASH       | Telegram API hash                                                                                  |
| RE_MONGO_CONNECT_URL | Connection URL for MongoDB is used in application (format: `mongodb://USERNAME:PASSWORD@IP:PORT/`) |
| RE_MONGO_DB_NAME     | Database name is used in MongoDB container startup and in the app                                  |
| RE_MONGO_USERNAME    | Username is used in MongoDB container startup                                                      |
| RE_MONGO_PASSWORD    | Password is used in MongoDB container startup                                                      |
| RE_MONGO_PORT        | Port is used in MongoDB container startup                                                          |
