FROM python:3.8-slim

WORKDIR /app

COPY . .

RUN pip install -e .

EXPOSE 8000

CMD ["uvicorn", "src.ua.s1ckretspace.realestate.tgxestate.webserver:app", "--host", "0.0.0.0", "--port", "8000"]
